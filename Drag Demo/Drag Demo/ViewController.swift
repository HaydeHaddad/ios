//
//  ViewController.swift
//  Drag Demo
//
//  Created by Hayden Haddad on 1/4/16.
//  Copyright © 2016 hayden. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let label = UILabel(frame: CGRectMake(self.view.bounds.width / 2 - 100, self.view.bounds.height / 2 - 50, 200, 100))
        
        label.text = "Drag me"
        label.textAlignment = NSTextAlignment.Center
        
        self.view.addSubview(label)
        
        let gesture = UIPanGestureRecognizer(target: self, action: Selector("wasDragged:"))
        
        label.addGestureRecognizer(gesture)
        label.userInteractionEnabled = true
    
    }
    
    func wasDragged (gesture: UIPanGestureRecognizer) {
        
        let translation = gesture.translationInView(self.view)
        let label = gesture.view!
        let xFormCenter = label.center.x - self.view.bounds.width / 2
        let scale = min(100/abs(xFormCenter), 1)
        var rotation = CGAffineTransformMakeRotation(xFormCenter / 200)
        var strech = CGAffineTransformScale(rotation, scale, scale)
        
        label.transform = strech
        
        label.center = CGPoint(x: self.view.bounds.width / 2 + translation.x, y: self.view.bounds.height / 2 + translation.y)
        
        if gesture.state == UIGestureRecognizerState.Ended {
            
            if label.center.x < 100 {
                
                print ("not chosen")
                
            } else if label.center.x > self.view.bounds.width - 100 {
                
                print ("Chosen")
            }
            
            rotation = CGAffineTransformMakeRotation(0)
            strech = CGAffineTransformScale(rotation, 1, 1)
            
            label.transform = strech
            
            label.center = CGPoint(x: self.view.bounds.width / 2 , y: self.view.bounds.height / 2)
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

