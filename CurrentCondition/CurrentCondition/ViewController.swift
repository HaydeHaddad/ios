//
//  ViewController.swift
//  CurrentCondition
//
//  Created by Hayden Haddad on 10/25/15.
//  Copyright © 2015 hayden. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    //let url = NSURL(string: "http://www.weather-forecast.com/locations/Paris/forecasts/latest")!
    
    @IBOutlet weak var textBox: UITextField!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var viewLabel: UILabel!
    
    @IBOutlet weak var imagePage: UIImageView!
    var wasSuccessful = false
    
    @IBAction func weatherButtom(sender: AnyObject) {
        
        let attemptedUrl =  NSURL(string: "http://www.weather-forecast.com/locations/" + textBox.text!.stringByReplacingOccurrencesOfString(" ", withString: "-") + "/forecasts/latest")
        if  let url = attemptedUrl {
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) -> Void in
            if let urlContent = data {
                
              let webContent = NSString(data:urlContent,encoding: NSUTF8StringEncoding)
               
              let webArray = webContent!.componentsSeparatedByString("3 Day Weather Forecast Summary:</b><span class=\"read-more-small\"><span class=\"read-more-content\"> <span class=\"phrase\">")
                
                if webArray.count > 1 {
                    let weatherArray = webArray[1].componentsSeparatedByString("</span>")
                    
                    if weatherArray.count > 1 {
                        
                        self.wasSuccessful = true
                        
                        let weatherSummary =  weatherArray[0].stringByReplacingOccurrencesOfString("&deg;", withString: "º")
                        
                        dispatch_async(dispatch_get_main_queue(),{ ()
                            
                        self.viewLabel.text = weatherSummary
                        })
                    }
                }
            }
            
            if self.wasSuccessful == false {
                
                self.viewLabel.text = "Could't find the weather for that city - please try again."
            }
        }
        
        task.resume()
        
        }else {
            self.viewLabel.text = "Could't find the weather for that city - please try again."
            
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let touch = touches.first {
            
            if touch.view != textBox{
                textBox.resignFirstResponder()
            }
        }
        super.touchesBegan(touches, withEvent:event)
    }
    
    
    override func viewDidLoad(){
        
        super.viewDidLoad()
      
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

