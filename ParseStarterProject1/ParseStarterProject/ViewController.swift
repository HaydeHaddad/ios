//
//  ViewController.swift
//
//  Copyright 2011-present Parse Inc. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
/*
        let product = PFObject(className: "Products")
        
        product["name"] = "Ic Creem"
        product["description"] = "vanila"
        product["price"] = 10.99

      product.saveInBackgroundWithBlock { (success, error) -> Void in

            print("Object has been saved.\(product.objectId)")
        }
*/
        
        let query = PFQuery(className: "Products")
        
        query.getObjectInBackgroundWithId ("gXBysl943d", block: { (object: PFObject?, error: NSError?) -> Void in
        
        
            if error != nil {
                print(error)
            } else {
                
                if let product = object {
                
                    product["description"] = "Orange"
                    product.saveInBackground()
                    
                //print(object)
               // print(object!.objectForKey("description"))
            }
        }
    })
}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

