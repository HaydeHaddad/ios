//
//  ViewController.swift
//  TableView
//
//  Created by Hayden Haddad on 10/18/15.
//  Copyright © 2015 hayden. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate {

    var names = ["Hayden","Ruby","Saari","Melek"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    public func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return names.count
    }
    
    public func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        
        cell.textLabel?.text = names[indexPath.row]
        
        return cell
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

