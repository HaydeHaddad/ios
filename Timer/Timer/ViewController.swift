//
//  ViewController.swift
//  Timer
//
//  Created by Hayden Haddad on 10/18/15.
//  Copyright © 2015 hayden. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var timer = NSTimer()
    var time = 0
    
    @IBOutlet weak var timerScreen: UILabel!
    
    func timerResult(){
        
        time++
        
        timerScreen.text = "\(time)"
        
    }
    
    @IBOutlet weak var play: UIBarButtonItem!
    
    @IBAction func play(sender: AnyObject) {
        
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(ViewController.timerResult), userInfo: nil, repeats: true)
        
    }

    @IBAction func stop(sender: AnyObject) {
        timer.invalidate()
        timerScreen.text = "0"
    }
    
    @IBAction func pause(sender: AnyObject) {
        timer.invalidate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }


}

