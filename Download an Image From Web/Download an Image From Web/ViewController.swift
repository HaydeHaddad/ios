//
//  ViewController.swift
//  Download an Image From Web
//
//  Created by Hayden Haddad on 11/17/15.
//  Copyright © 2015 hayden. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var image: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        let imageUrl = NSURL(string: "https://media.licdn.com/mpr/mpr/shrinknp_400_400/p/1/005/0ac/2f1/09e40e8.jpg")
        
       let urlRequest = NSURLRequest(URL: imageUrl!)
        
        NSURLConnection.sendAsynchronousRequest(urlRequest, queue: NSOperationQueue.mainQueue()) { (response, data , error) -> Void in

            
            if error != nil {
                
                print(error)
                
            } else {
                
                if let myPic = UIImage(data: data!) {
                    
                   // self.image.image = myPic
                    
                    let documentDirectory:String?
                    
                    let paths:[AnyObject] = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
                    
                    if paths.count > 0 {
                        
                        documentDirectory = paths[0] as! String
                        
                        let savePath = documentDirectory! + "/myPic.jpg"
                        
                        NSFileManager.defaultManager().createFileAtPath(savePath, contents: data, attributes: nil)
                    
                        //to return image from your local
                        self.image.image = UIImage(named: savePath)
                    }
                }
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

