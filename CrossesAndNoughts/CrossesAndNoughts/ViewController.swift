//
//  ViewController.swift
//  CrossesAndNoughts
//
//  Created by Hayden Haddad on 11/1/15.
//  Copyright © 2015 hayden. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var activePlayer = 1
    var gameState = [0, 0, 0, 0, 0, 0 ,0 ,0 ,0]
    var winningCombinations = [[0, 1, 2],[3, 4, 5],[6, 7, 8],[0, 3, 6],[1, 4, 7],[2, 5, 8],[2, 4, 6],[0, 4, 8]]
    var gameActive = true
    
    @IBOutlet weak var playAgain: UIButton!
    
    @IBAction func playAgainPress(sender: AnyObject) {
        
        gameState = [0, 0, 0, 0, 0, 0 ,0 ,0 ,0]
        activePlayer = 1
        
        gameOverLabel.hidden = true
        gameActive = true
        gameOverLabel.center = CGPointMake(gameOverLabel.center.x - 500, gameOverLabel.center.y)
        
        playAgain.hidden = true
        playAgain.center = CGPointMake(playAgain.center.x - 500, playAgain.center.y)
        
        var buttonToClear : UIButton
        
        for var i = 0; i < 9; i++ {
            
            buttonToClear = view.viewWithTag(i) as! UIButton
            buttonToClear.setImage(nil, forState: .Normal)
        }
    }
    
    
    @IBOutlet weak var button: UIButton!
    
    @IBOutlet weak var gameOverLabel: UILabel!
    
    
    @IBAction func buttonPressed(sender: AnyObject) {
        
        if (gameState[sender.tag] == 0 && gameActive == true) {
            
            gameState[sender.tag] = activePlayer
        if activePlayer == 1 {
            
        sender.setImage(UIImage(named: "greenDought.png"), forState: .Normal)
        activePlayer = 2
            
     }else
        {
            sender.setImage(UIImage(named: "bCross.png"), forState: .Normal)
            activePlayer = 1
        }
            
        for combination in winningCombinations {
                
            if (gameState[combination[0]] != 0 && gameState[combination[0]] == gameState[combination[1]] && gameState[combination[1]] == gameState[combination[2]]){
                
                gameActive = false
                    if gameState[combination[0]] == 1 {
                        
                        gameOverLabel.text = "Doughts have win"
                        
                    } else {
                        gameOverLabel.text = "Crosses have win"
                        
                    }
                
                endGame()
            }
         }
            if gameActive == true {
                
                gameActive = false
            
                for buttonState in gameState {
                
                    if buttonState == 0 {
                        gameActive = true
                    }
                }
            
                if gameActive == false {
                
                    gameOverLabel.text = "It's a draw"
                    endGame()
                }
            }
      }
    }
    
    
    override func viewDidLoad() {

        super.viewDidLoad()
        gameOverLabel.hidden = true
        gameOverLabel.center = CGPointMake(gameOverLabel.center.x - 500, gameOverLabel.center.y)
        
        playAgain.hidden = true
        playAgain.center = CGPointMake(playAgain.center.x - 500, gameOverLabel.center.y)
    
    }
    
    func endGame() {
        gameOverLabel.hidden = false
        playAgain.hidden = false
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.gameOverLabel.center = CGPointMake(self.gameOverLabel.center.x + 500, self.gameOverLabel.center.y)
            
            self.playAgain.center = CGPointMake(self.playAgain.center.x + 500, self.playAgain.center.y)
            
        })
    }

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

