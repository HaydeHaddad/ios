//
//  ViewController.swift
//  WorkWithData
//
//  Created by Hayden Haddad on 11/15/15.
//  Copyright © 2015 hayden. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
 
        let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context: NSManagedObjectContext = appDel.managedObjectContext
        
       // comment out this sectio ones you add and save data
    /*
        let newUser = NSEntityDescription.insertNewObjectForEntityForName("Users", inManagedObjectContext: context)
        newUser.setValue("Saari", forKey: "userName")
        newUser.setValue("123Hayden:", forKey: "password")
        
        do {
            try context.save()
            
        }catch {
            print("There was a problem")
        }

        */
        let request = NSFetchRequest(entityName: "Users")
        
        //request.predicate = NSPredicate(format: "userName = %@", "Saari")
        
        request.returnsObjectsAsFaults = false
        
        do {
            
            let results = try context.executeFetchRequest(request)
            
            
            if results.count > 0 {
                
                for result in results as! [NSManagedObject] {
                    
        //Delete
            /*        context.deleteObject(result)
                    
                    do {
                        try context.save()
                        
                    }catch {
                        
                    }
    */
                    
        /*  //Update
                    result.setValue("Melek", forKey: "userName")
        */
                    if let  userName = result.valueForKey("userName") as? String {
                    
                    print(userName)
                    }
                }
            }
            
        } catch {
            print("fetch Failed")
        }
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

