//
//  SwipViewController.swift
//  ParseStarterProject
//
//  Created by Hayden Haddad on 1/24/16.
//  Copyright © 2016 Parse. All rights reserved.
//

import UIKit
import Parse

class SwipViewController: UIViewController {
    
    
    @IBOutlet weak var swipLable: UIImageView!

    @IBOutlet weak var userImage: UIImageView!
    
    var displayUserId = ""
    
    func wasDragged (gesture: UIPanGestureRecognizer) {
        
        let translation = gesture.translationInView(self.view)
        let label = gesture.view!
        let xFormCenter = label.center.x - self.view.bounds.width / 2
        let scale = min(100/abs(xFormCenter), 1)
        var rotation = CGAffineTransformMakeRotation(xFormCenter / 200)
        var strech = CGAffineTransformScale(rotation, scale, scale)
        
        label.transform = strech
        
        label.center = CGPoint(x: self.view.bounds.width / 2 + translation.x, y: self.view.bounds.height / 2 + translation.y)
        
        if gesture.state == UIGestureRecognizerState.Ended {
            
            var acceptedOrRejected = ""
            
            if label.center.x < 100 {
                
                acceptedOrRejected = "rejected"
                
            } else if label.center.x > self.view.bounds.width - 100 {
                

                acceptedOrRejected = "accepted"
                
            }
            
            if acceptedOrRejected != "" {
                
               //PFUser.currentUser()?[acceptedOrRejected] = displayUserId
                PFUser.currentUser()?.addUniqueObjectsFromArray([displayUserId],forKey:acceptedOrRejected)
                PFUser.currentUser()?.save()
            }
            
            rotation = CGAffineTransformMakeRotation(0)
            strech = CGAffineTransformScale(rotation, 1, 1)
            
            label.transform = strech
            
            label.center = CGPoint(x: self.view.bounds.width / 2 , y: self.view.bounds.height / 2)
            
            updateImage()
            
        }
        
    }
    
    
    func updateImage () {
        
        
        let query = PFUser.query()!
        var interestedIn = "male"
        
        if PFUser.currentUser()!["interestedInWomen"]! as! Bool == true {
            
            interestedIn = "female"
        }
        
        var isFemale = true
        
        if PFUser.currentUser()!["gender"]! as! String == "male" {
            
            isFemale = false
        }
        
        query.whereKey("gender", equalTo: interestedIn)
        query.whereKey("interestedInWomen", equalTo: isFemale)
        
        var ignouredUsers = [""]
        
        if let acceptedUsers = PFUser.currentUser()?["accepted"] {
            
            ignouredUsers += acceptedUsers as! Array
        }
        
        if let rejectedUsers = PFUser.currentUser()?["rejected"] {
            
            ignouredUsers  += rejectedUsers as! Array
        }
        
        query.whereKey("objectId", notContainedIn: ignouredUsers)
        
        query.limit = 1
        
        query.findObjectsInBackgroundWithBlock {
            
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error != nil {
                
                print(error)
                
            } else if let objects = objects {
                
                for object in objects {
                    
                    self.displayUserId = object.objectId!!
                    
                    let imageFile = object["image"] as! PFFile
                    
                    imageFile.getDataInBackgroundWithBlock  {
                        
                        (imageData: NSData?, error: NSError?) -> Void in
                        
                        if error != nil {
                            
                            print(error)
                            
                        } else {
                            
                            if let data = imageData {
                                self.userImage.image = UIImage(data: data)
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let gesture = UIPanGestureRecognizer(target: self, action: Selector("wasDragged:"))
        
        self.userImage.addGestureRecognizer(gesture)
        self.userImage.userInteractionEnabled = true
        
        updateImage()
        

        
        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier ==  "logOut" {
            
            PFUser.logOut()
            
        }
        
    }
 

}
