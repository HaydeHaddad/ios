//
//  SignUpViewController.swift
//  ParseStarterProject
//
//  Created by Hayden Haddad on 1/19/16.
//  Copyright © 2016 Parse. All rights reserved.
//

import UIKit
import Parse

class SignUpViewController: UIViewController {
    
    
    @IBAction func signUp(sender: AnyObject) {
        
        PFUser.currentUser()?["interestedInWomen"] = intrestedInWomen.on
        PFUser.currentUser()?.save()
    }
    
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var intrestedInWomen: UISwitch!
    

    override func viewDidLoad() {
        super.viewDidLoad()
     
       /* -- add users-----
        var imageUrls = ["http://www.firsthdwallpapers.com/uploads/2013/05/new-3d-and-fantasy-girls-HD-wallpaper.jpg"
            ,"http://images.alphacoders.com/463/463576.jpg",
            "http://freedwallpaper.com/wp-content/uploads/2014/11/girl-wallpapers-hd.jpg",
            "http://www.dereksmith.com/wp-content/uploads/2011/12/female-fashion-professional.jpg"]
       
        
        var counter = 1
        
        for url in imageUrls {
            
            let nsUrl = NSURL(string: url)!
            
            if let data = NSData(contentsOfURL: nsUrl) {
                
                self.userImage.image = UIImage(data: data)
                
                let imageFile: PFFile = PFFile(data: data)
                
                //PFUser.currentUser()?["image"] = imageFile
                
                let user:PFUser = PFUser()
                
                let username = "user\(counter)"
                user.username = username
                user.password = "pass"
                user["image"] = imageFile
                user["interestedInWomen"] = false
                user["gender"] = "female"
                counter++
                user.signUp()

            }
        }
        --------- Add users
*/
        
        let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters : ["fields": "id, name, gender"])
        graphRequest.startWithCompletionHandler ( {
            
            (connection, result, error) -> Void in
            
            if error != nil {
                
                print(error)
                
            } else if let result = result {
                
                PFUser.currentUser()?["gender"] = result["gender"]
                PFUser.currentUser()?["name"] = result["name"]
                
                PFUser.currentUser()?.save()
                
                let userId = result["id"] as! String
                let facebookProfilePictureStringUrl = "https://graph.facebook.com/" + userId + "/picture?type=large"
                
                let fbPicUrl = NSURL(string: facebookProfilePictureStringUrl)
                
                if let data = NSData(contentsOfURL: fbPicUrl!) {
                    
                    self.userImage.image = UIImage(data: data)
                    
                    let imageFile: PFFile = PFFile(data: data)
                    
                    PFUser.currentUser()?["image"] = imageFile
                    
                    PFUser.currentUser()?.save()
                }
                
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
