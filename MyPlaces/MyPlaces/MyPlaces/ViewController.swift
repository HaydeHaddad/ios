//
//  ViewController.swift
//  MyPlaces
//
//  Created by Hayden Haddad on 11/7/15.
//  Copyright © 2015 hayden. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, CLLocationManagerDelegate {
    

    
    var manager: CLLocationManager!
    
    @IBOutlet weak var map: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        
        if (activePlace == -1){
            
            manager.requestWhenInUseAuthorization()
            manager.startUpdatingLocation()
    
        }else{
            
            let latitude = NSString(string:places[activePlace]["lat"]!).doubleValue
            let longitude = NSString(string:places[activePlace]["lon"]!).doubleValue
            let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
            let latDelta:CLLocationDegrees = 0.01
            let longDelta:CLLocationDegrees = 0.01
            let span:MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: longDelta)
            let region:MKCoordinateRegion = MKCoordinateRegion(center: coordinate, span: span)
            
            self.map.setRegion(region, animated: true)
            
            let  annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            annotation.title = places[activePlace]["name"]
            self.map.addAnnotation(annotation)
        }
        


        
        let uilpgr = UILongPressGestureRecognizer(target: self, action: "action:")
        uilpgr.minimumPressDuration = 2
        map.addGestureRecognizer(uilpgr)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func action(gestureRecognizer:UIGestureRecognizer) {
        
        if gestureRecognizer.state == UIGestureRecognizerState.Began{
            
            let touchPoint = gestureRecognizer.locationInView(map.self)
            
            let newCoordinate = self.map.convertPoint(touchPoint, toCoordinateFromView: map.self)
            
            let location = CLLocation(latitude: newCoordinate.latitude, longitude: newCoordinate.longitude)
            
            CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
                
            var title = ""
                
                if (error == nil){
                    
                    if let p = CLPlacemark(placemark: (placemarks?[0])!) as CLPlacemark!{
                        
                        //print(placemarks)
                        
                        var subThoroughfare:String = ""
                        var thoroughfare:String = ""
                        var locality:String = ""
                        var postalCode:String = ""
                       
                        if p.subThoroughfare != nil {
                            
                            subThoroughfare = p.subThoroughfare!
                            
                        }
                        
                        if p.thoroughfare != nil {
                            
                            thoroughfare = p.thoroughfare!
                            
                        }
                        
                        if p.locality != nil {
                            locality = p.locality!
                        }
                        
                        if p.postalCode != nil {
                            postalCode = p.postalCode!
                        }
                        
                       // \(p.subAdministrativeArea!) \n \(p.locality!) \n \(p.postalCode!)
                        
                        title = "\(subThoroughfare) \(thoroughfare) \(locality) \(postalCode)"
                        
                    }
                    
                }
                if title == "" {
                    
                    title = "Added \(NSData())"
                }
                places.append(["name":title, "lat":"\(newCoordinate.latitude)", "lon":"\(newCoordinate.longitude)" ])
                
                let  annotation = MKPointAnnotation()
                
                annotation.coordinate = newCoordinate
                
                annotation.title = title
                
                self.map.addAnnotation(annotation)
                NSUserDefaults.standardUserDefaults().setObject(places, forKey: "name")
                
            })

        }

    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    
        let userLocation:CLLocation = locations[0]
        let latitude = userLocation.coordinate.latitude
        let longitude = userLocation.coordinate.longitude
        let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        let latDelta:CLLocationDegrees = 0.01
        let longDelta:CLLocationDegrees = 0.01
        let span:MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: longDelta)
        let region:MKCoordinateRegion = MKCoordinateRegion(center: coordinate, span: span)
        
        self.map.setRegion(region, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

