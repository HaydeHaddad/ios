//
//  textInputTableViewCell.swift
//  MyPlaces
//
//  Created by Hayden Haddad on 2/7/16.
//  Copyright © 2016 hayden. All rights reserved.
//

import UIKit

public class textInputTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var textField: UITextField!
    
    public func configure (text: String?, placeHolder: String ) {
        
        textField.text = text
        
        textField.placeholder = placeHolder
        
        textField.accessibilityValue = text
        
        textField.accessibilityLabel = placeHolder
        
    }
    

    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textField.text = textField.text
        NSUserDefaults.standardUserDefaults().setObject(places, forKey: "name")
    }

    override public func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
