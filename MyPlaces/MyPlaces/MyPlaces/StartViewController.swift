//
//  StartViewController.swift
//  MyPlaces
//
//  Created by Hayden Haddad on 11/11/15.
//  Copyright © 2015 hayden. All rights reserved.
//

import UIKit
import AVFoundation
class StartViewController: UIViewController {
    
    var audioPlayer:AVAudioPlayer = AVAudioPlayer()

    @IBAction func playButton(sender: AnyObject) {
        audioPlayer.play()
    }
    
    @IBAction func pausButton(sender: AnyObject) {
        audioPlayer.pause()
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        do {
            
            try audioPlayer = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("PlayAud", ofType: "mp3")!))
        } catch {
            
        }
        
        audioPlayer.play()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        audioPlayer.pause()
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }


}
