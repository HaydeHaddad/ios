//
//  SecondViewController.swift
//  ToDoList
//
//  Created by Hayden Haddad on 10/22/15.
//  Copyright © 2015 hayden. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController{

    @IBOutlet weak var toDoText: UITextField!
    
    @IBAction func addItemButtom(sender: AnyObject) {
        if toDoText.text != "" {
        toDoList.append(toDoText.text!)
        toDoText.text = ""
        NSUserDefaults.standardUserDefaults().setObject(toDoList, forKey: "toDoList")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let touch = touches.first {
            
            if touch.view != toDoText{
                toDoText.resignFirstResponder()
        }
       }
        super.touchesBegan(touches, withEvent:event)
    }
    
}

