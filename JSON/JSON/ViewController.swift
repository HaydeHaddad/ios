//
//  ViewController.swift
//  JSON
//
//  Created by Hayden Haddad on 11/19/15.
//  Copyright © 2015 hayden. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let url  = NSURL(string: "https://freegeoip.net/json/")!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (data, respons, error) -> Void in
            
            if let urlContent = data {
                
                do {
                    
                   let jsonResult =  try NSJSONSerialization.JSONObjectWithData(urlContent, options: NSJSONReadingOptions.MutableContainers)
                    
                    print (jsonResult)
                } catch {
                    
                    print("Error Json")
                }
              
            }
        }
        task.resume()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

