//
//  GameViewController.swift
//  SaveThePlan
//
//  Created by Hayden Haddad on 3/13/16.
//  Copyright (c) 2016 Hayden Haddad. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation


class GameViewController: UIViewController {
 
   var backgroundMusicPlayer: AVAudioPlayer!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        if let scene = GameScene(fileNamed:"GameScene") {
            // Configure the view.
            let skView = self.view as! SKView
            skView.showsFPS = true
            skView.showsNodeCount = true
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
           
            //scene.size = skView.bounds.size
            skView.presentScene(scene)
            
            //playBackgroundMusic("exciting.mp3")
        }
    }
    
    
    func playBackgroundMusic(filename: String) {
        let url = NSBundle.mainBundle().URLForResource(
            filename, withExtension: nil)
        if (url == nil) {
            print("Could not find file: \(filename)")
            return
        }
        
        let error: String? = nil
        
        do {
            try  backgroundMusicPlayer = AVAudioPlayer(contentsOfURL: url!, fileTypeHint: filename)//(contentsOfURL: url, error: &error)
            if backgroundMusicPlayer == nil {
                print("Could not create audio player: \(error!)")
                return
            }
        
            backgroundMusicPlayer.numberOfLoops = -1
            backgroundMusicPlayer.prepareToPlay()
            backgroundMusicPlayer.play()
        
        } catch {
            
        }

    }
    
    

    override func shouldAutorotate() -> Bool {
        return false
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
