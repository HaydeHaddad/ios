//
//  GameScene.swift
//  SaveThePlan
//
//  Created by Hayden Haddad on 3/13/16.
//  Copyright (c) 2016 Hayden Haddad. All rights reserved.
//

import SpriteKit
import AVFoundation

class GameScene: SKScene, SKPhysicsContactDelegate
{

    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    var sound = SKAction.playSoundFileNamed("explosion.mp3", waitForCompletion: false)

    var score = 0
    var scoreLabel = SKLabelNode()
    
    var plan = SKSpriteNode()
    
    var bg = SKSpriteNode()
    
    var twist = SKSpriteNode()
    
    var twistRun:SKAction!
    
    var labelsContainer = SKSpriteNode()
    
    var objectsContainer = SKSpriteNode()
    
    var timer: NSTimer! = nil
    
    var time = 0
    
    var backgroundMusic: SKAudioNode!
    
    var counter = 1
    
    var bgTexture = SKTexture(imageNamed: "")
        
    var StartLabel =  SKLabelNode(text:"")
    
    var animation = SKAction.animateWithTextures([],timePerFrame: 0)

    
    enum ColliderType: UInt32 {
        
       case Plan = 1
       case Object = 2
       case Gap = 4
    }
    
    var gameOver = false
    
    
    func addBackgroupMusic(fileName: String, ext : String) {
        
        if let musicURL = NSBundle.mainBundle().URLForResource(fileName, withExtension: ext) {
            backgroundMusic = SKAudioNode(URL: musicURL)
            
            if backgroundMusic != nil {
                addChild(backgroundMusic)
            } else {
                
                print("File was not found")
            }
            
        }
        
    }
    
    func starterHelp() {
        
        StartLabel =  SKLabelNode(text:"Tap to move the plane!")
        StartLabel.fontName = "Chalkduster"
        StartLabel.fontSize = 43
        StartLabel.fontColor = UIColor.blueColor()
        StartLabel.position = CGPoint(x: frame.size.width/2, y: frame.size.height * 0.6)
        StartLabel.zPosition = 10
        self.addChild(StartLabel)
    }
    
    
    
    func bestTime(color: UIColor, fountSize: CGFloat, fountName: String ) {
        
        let bestTime: String = String(saveSurvivalTime(time))
        
        let bestTimeLable = SKLabelNode(text: "(" + bestTime + ")!")
        bestTimeLable.fontName = fountName
        bestTimeLable.fontSize = fountSize
        bestTimeLable.fontColor =  color
        bestTimeLable.position = CGPoint(x: frame.size.width/2, y: frame.size.height * 0.6)
        bestTimeLable.zPosition = 8
        labelsContainer.addChild(bestTimeLable)
        
    }
    
   
    
    func gameoverMessage() {
        
        
     

        let gameoverLabel =  SKLabelNode(text:"Gameover!")
        gameoverLabel.fontName = "Chalkduster"
        gameoverLabel.fontSize = 55
        gameoverLabel.fontColor = UIColor.redColor()
        gameoverLabel.position = CGPoint(x: frame.size.width/2, y: frame.size.height * 0.85)
        gameoverLabel.zPosition = 5
        
        let gameoverLabel11 =  SKLabelNode(text:"Current Score:" + scoreLabel.text!)
        gameoverLabel11.fontName = "Chalkduster"
        gameoverLabel11.fontSize = 35
        gameoverLabel11.fontColor = UIColor.redColor()
        gameoverLabel11.position = CGPoint(x: frame.size.width/2, y: frame.size.height * 0.8)
        gameoverLabel11.zPosition = 11
        
        let gameoverLabel2 = SKLabelNode(text: "Tap to play again")
        gameoverLabel2.fontName = "Chalkduster"
        gameoverLabel2.fontSize = 55
        gameoverLabel2.fontColor = UIColor.redColor()
        gameoverLabel2.position = CGPoint(x: frame.size.width/2, y: frame.size.height * 0.5)
        gameoverLabel2.zPosition = 6

        let highScoreLable = SKLabelNode(text: "Your best time is ")
        highScoreLable.fontName = "Chalkduster"
        highScoreLable.fontSize = 45
        highScoreLable.fontColor = UIColor.blueColor()
        highScoreLable.position = CGPoint(x: frame.size.width/2, y: frame.size.height * 0.7)
        highScoreLable.zPosition = 7
        

        
        let OverHandred = SKLabelNode(text: "Excellent!! You reached over ")
        OverHandred.fontName = "Chalkduster"
        OverHandred.fontSize = 33
        OverHandred.fontColor = UIColor.brownColor()
        OverHandred.position = CGPoint(x: frame.size.width/2, y: frame.size.height * 0.7)
        OverHandred.zPosition = 9
        
        
        StartLabel.removeFromParent()
        
        labelsContainer.addChild(gameoverLabel)
        labelsContainer.addChild(gameoverLabel2)
    
        if time >= 100 {
            labelsContainer.addChild(OverHandred)
            bestTime(UIColor.brownColor(), fountSize: 60, fountName: "Chalkduster")
        } else {
            labelsContainer.addChild(highScoreLable)
            bestTime(UIColor.blueColor(), fountSize: 60, fountName: "Chalkduster")
            labelsContainer.addChild(gameoverLabel11)
        }
        
        
    }
    
    

    //--------------------Background Fun--------------------------------
    
    func background(bgName: String) {
    
            bgTexture = SKTexture(imageNamed: bgName)
        
        let movebg = SKAction.moveByX(-bgTexture.size().width, y: 0, duration: 9)
        
        let repalcebg = SKAction.moveByX(bgTexture.size().width, y: 0, duration: 0)
        
        let movebgforever = SKAction.repeatActionForever(SKAction.sequence([movebg, repalcebg]))
        
        
        for i: Int in 0 ..< 3 {
            
            
            bg = SKSpriteNode(texture: bgTexture)
            
            bg.position = CGPoint(x: bgTexture.size().width/2 + bgTexture.size().width * CGFloat(i), y: CGRectGetMidX(self.frame))
            
            bg.size.height = self.frame.height
            
            bg.zPosition = 1
            
            bg.runAction(movebgforever)
            
            objectsContainer.addChild(bg)
            
        }
    }
    
    
    
    
  // ----------------------------add plan fun began ------------------------
    func addPlan(planName: String) {
        
        let planTexture = SKTexture(imageNamed: planName)
        let planTexture1 = SKTexture(imageNamed: "plan-17.png")
        let planTexture2 = SKTexture(imageNamed: "plan-18.png")
        
    
        animation = SKAction.animateWithTextures([planTexture,planTexture1,planTexture2,planTexture1], timePerFrame: 0.9)
    
        let makeplanFly = SKAction.repeatActionForever(animation)
    
        plan = SKSpriteNode(texture: planTexture)

        plan.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidX(self.frame))
    
        plan.runAction(makeplanFly)
    
        plan.physicsBody = SKPhysicsBody(circleOfRadius: planTexture.size().height/2)
        
        plan.zPosition = 2
    
        plan.physicsBody!.categoryBitMask = ColliderType.Plan.rawValue
        plan.physicsBody!.contactTestBitMask = ColliderType.Object.rawValue
        plan.physicsBody!.collisionBitMask = ColliderType.Object.rawValue
    
        self.addChild(plan)
    }
    
   
    
    
    //---------------------addGround func -------------------------------
    func addGround() {
        let ground = SKNode()
        ground.position = CGPointMake(CGRectGetMidX(self.frame), 110)
        ground.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(self.frame.size.width , 1))
        ground.physicsBody?.dynamic = false
    
        ground.physicsBody!.categoryBitMask = ColliderType.Object.rawValue
        ground.physicsBody!.contactTestBitMask = ColliderType.Object.rawValue
        ground.physicsBody!.collisionBitMask = ColliderType.Object.rawValue
    
    self.addChild(ground)
    }
    //---------------------End Ground fuc -------------------------------
  
    func addSky() {
        
        let sky = SKNode()
        
        sky.position = CGPointMake(1010  , 1010)
        sky.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(self.frame.size.width, 1))
        sky.physicsBody?.dynamic = false
        
        
        self.addChild(sky)
        
    }
  
    
    
    
    //--------------- addTwist func -------------------------
    
    func addTwist(twistHight: CGFloat, imageName: String, duration: CGFloat) {
        
        let frameHight = self.frame.size.height
        let frameWidth = self.frame.size.width
        let gapHight = plan.size.height * 4
        let movmentAmount = arc4random() % UInt32(frameHight / 2)
        let twistOffset = CGFloat(movmentAmount) - (frameHight / twistHight)
        let midScreenX = CGRectGetMidX(self.frame)
        let midScreenY = CGRectGetMidY(self.frame)
        let moveTwist = SKAction.moveByX(-frameWidth * 2, y: 0,  duration: NSTimeInterval(frameWidth / duration))
        let removeTwist = SKAction.removeFromParent()
        let moveAndRemoveTwists = SKAction.sequence([moveTwist,removeTwist])
        
        let twistTextur = SKTexture(imageNamed: imageName)
        twist = SKSpriteNode(imageNamed: imageName)
        twist.position = CGPoint(x:midScreenX + frameWidth , y: midScreenY + twistTextur.size().height / 2 + gapHight / 2 + twistOffset )
        twist.zPosition = 3
        twist.runAction(moveAndRemoveTwists)
        
        twist.physicsBody = SKPhysicsBody(circleOfRadius: twistTextur.size().height-98)
        
        twist.physicsBody?.dynamic = false
        twist.physicsBody?.allowsRotation = false
        twist.physicsBody?.categoryBitMask = ColliderType.Object.rawValue
        twist.physicsBody?.contactTestBitMask = ColliderType.Object.rawValue
        twist.physicsBody?.collisionBitMask = ColliderType.Object.rawValue
    
        objectsContainer.addChild(twist)
        
        animateObject()

    }
    
    
    func addTwistWithParamValue1() {
        
        addTwist(2, imageName: "twist-1.png", duration: 98)
    }
    
    func addTwistWithParamValue2() {
        
        addTwist(3,imageName: "twist-1.png", duration: 90)
    }
    
    func addTwistWithParamValue3() {
        
        addTwist(3,imageName: "twist-1.png", duration: 95)
    }
    
    func addTwistWithParamValue4() {
        
        addTwist(2,imageName: "twist-1.png", duration: 100)
    }
    
 
    func  animateObject(){

    let atlas = SKTextureAtlas(named: "twistImages")
        
    let anim = SKAction.animateWithTextures([
        atlas.textureNamed("twist-1.png"),
        atlas.textureNamed("twist-2.png"),
        atlas.textureNamed("twist-3.png"),
        atlas.textureNamed("twist-4.png"),
        atlas.textureNamed("twist-5.png")], timePerFrame: 0.1),
    
    twistRun = SKAction.repeatActionForever(anim)
    twist.runAction(twistRun)
    
    }
    
    
    func playSound(sound: SKAction){
    
        runAction(sound)
    }
    
    
    func screenWidth(x:Float) -> Float {
        
        return (x * Float(self.frame.width))
    }
    
    func screenHight(y:Float) -> Float {
        
        return (y * Float(self.frame.height))
    }

    //-----------------------------------------------------------------------------------  View did move ---------------------------------------------------//
    
    
    override func didMoveToView(view: SKView) {
        
        defaults.setValue(0, forKey: "scoreTime")
        
        self.physicsWorld.contactDelegate = self
        
        addBackgroupMusic("exciting",ext: "mp3")
        
        addPlan("plan-16.png")
        
        plan.physicsBody!.affectedByGravity = false
     
        addGround()
        
        addSky()
        
        background("bg3.png")
        
        addChild(objectsContainer)
        addChild(labelsContainer)
        
        scoreLabel.fontName = "Chalkduster"
        scoreLabel.fontSize = 50
        scoreLabel.text = "0"
        scoreLabel.fontColor = UIColor.blueColor()
        scoreLabel.position = CGPointMake(CGRectGetMidX(self.frame), self.frame.size.height - 70)
        scoreLabel.zPosition = 4
        
        self.addChild(scoreLabel)
        
        starterHelp()
        
        
           gameTimer()
           twistAttackLevel()
    }
    

    
    
    func twistAttackLevel() {
        
            _ = NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: #selector(GameScene.addTwistWithParamValue1), userInfo: nil, repeats: true)
            _ = NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: #selector(GameScene.addTwistWithParamValue2), userInfo: nil, repeats: true)
    }
    
    
    
    func didBeginContact(contact: SKPhysicsContact) {
        
            
        if contact.bodyA.categoryBitMask == ColliderType.Object.rawValue || contact.bodyB.categoryBitMask == ColliderType.Object.rawValue {
            
            
            if gameOver == false {
                
                
                timer.invalidate()
                
                playSound(sound)
                
                backgroundMusic.runAction(SKAction.stop())
                
                gameOver = true
                
                self.speed = 0
                
                gameoverMessage()
                
                saveSurvivalTime(time)
                
            }
        }
    }
    
 //---------------------------Save time -------------------
 
  
    func saveSurvivalTime(survivalTime:Int) -> Int {
        
        var score1: Int = 0
        
            let currentSurvivalTime = (defaults.valueForKey("scoreTime") as? Int)!
            
            if (survivalTime > currentSurvivalTime){
                
                defaults.setInteger(survivalTime, forKey: "scoreTime")
                
                 score1 = defaults.integerForKey("scoreTime")
                
                 return score1
            
        }
        return currentSurvivalTime
    }
 //---------------------------- timer --------------------

  
    func timerResult(){
        
        time+=1
        
        scoreLabel.text = "\(time)"
    }
    
    
    func gameTimer() {

        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(GameScene.timerResult), userInfo: nil, repeats: true)
        
    }

    

   //----------------------------------------------------
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    
        if gameOver == false {

                plan.physicsBody!.velocity = CGVectorMake(0, 0)
                plan.physicsBody!.applyImpulse(CGVectorMake(0, 250))
                plan.physicsBody?.allowsRotation = false
                plan.physicsBody!.affectedByGravity = true
                StartLabel.removeFromParent()
            
            if  time >= 20 {
               
    
                if time % 3 == 0  {
                
                    NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: #selector(GameScene.addTwistWithParamValue4), userInfo: nil, repeats: false)
                    
                }
                
                
            }
            
            
        } else {
           
            
            addBackgroupMusic("exciting",ext: "mp3")
            
            
            gameTimer()
            
            time = 0
            
            scoreLabel.text = "0"
            
            plan.physicsBody!.velocity = CGVectorMake(0, 0)
            
            plan.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
            
            plan.physicsBody!.velocity = CGVectorMake(0, 0)
            
            objectsContainer.removeAllChildren()
         
            background("bg3.png")
            
            self.speed = 1
            
            gameOver = false
            
            
            labelsContainer.removeAllChildren()
            
        }

    }
    

    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        
    }
}
