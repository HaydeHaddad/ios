//
//  StartViewController.swift
//  SaveThePlan
//
//  Created by Hayden Haddad on 5/2/16.
//  Copyright © 2016 Hayden Haddad. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    
    var counter = 1
    var timer = NSTimer()
    var isAnimating = true

    @IBOutlet weak var image: UIImageView!
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

         timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector:#selector(StartViewController.doAnimation), userInfo: nil, repeats: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    

    func doAnimation() {
        
        if  counter == 18 {
            counter = 1
        } else
        {
            counter += 1
            image.image = UIImage(named: "plan-\(counter).png")
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
