//
//  ViewController.swift
//  How Many Fingurs
//
//  Created by Hayden Haddad on 10/6/15.
//  Copyright © 2015 hayden. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textbox: UITextField!
    
    @IBOutlet weak var returnResults: UILabel!
    
    
    
    @IBAction func press(sender: AnyObject) {
        
        
        if let enteredText = Int(textbox.text!)
        {
            
            let randomNo = Int(arc4random_uniform(6))
            
            if (randomNo == enteredText)
              {
                returnResults.text = "Correct"
              }
                
        else {
                returnResults.text = " Not a good guess, it was \(randomNo)"
             }
        }
        
        else
        
        {
            print("Please enter a whole number")
        }
        
        
    }
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
