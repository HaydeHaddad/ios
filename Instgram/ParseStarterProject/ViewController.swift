//
//  ViewController.swift
//
//  Copyright 2011-present Parse Inc. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController {

    var signupActive = true
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var logButton: UIButton!
    @IBOutlet weak var registerButtom: UILabel!
    @IBOutlet weak var signupButton: UIButton!
    
    
    
    @IBAction func signUp(sender: AnyObject) {
        
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        
        func displayAlert(title: String, message: String){
          
            var alert  = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction((UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                self.dismissViewControllerAnimated(true, completion: nil)
                
            })))
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        if userName.text == "" || password.text == "" {
            
            displayAlert("Login Error", message: "Please enter a username and password")
            
        }else {
            
            activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
            view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            
            var errorMessage = "Please try again later"
            
            if signupActive == true {
            
            var user = PFUser()
            user.username = userName.text
            user.password = password.text
            
            
            user.signUpInBackgroundWithBlock( { (success, error) -> Void in
                
                activityIndicator.stopAnimating()
                UIApplication.sharedApplication().endIgnoringInteractionEvents()
                
                if error == nil {
                    
                    //signup
                    self.performSegueWithIdentifier("login", sender: self)
                    
                    
                } else {
                    
                    if let errorString = error!.userInfo["error"] as? String {
                        
                        errorMessage = errorString
                    }
                    
                    displayAlert("Signup Failed", message: errorMessage)
                    
                }
                
            })
            } else {
                
                PFUser.logInWithUsernameInBackground(userName.text!, password: password.text!, block: {(user, error) -> Void in
                    
                    activityIndicator.stopAnimating()
                    UIApplication.sharedApplication().endIgnoringInteractionEvents()
                    
                    if user != nil {
                        // logged In
                        
                        self.performSegueWithIdentifier("login", sender: self)
                        
                    } else {
                        
                        if let errorString = error!.userInfo["error"] as? String {
                            
                            errorMessage = errorString
                        }
                        
                        displayAlert("Login Failed", message: errorMessage)
                        
                    }
                })
            }
        }

    }
    
    @IBAction func login(sender: AnyObject) {

        if signupActive == true {
            
            logButton.setTitle("Login", forState: UIControlState.Normal)
            registerButtom.text = "Not Register!"
            signupButton.setTitle("Sign Up", forState: UIControlState.Normal)
            signupActive = false
        } else {
            
            logButton.setTitle("Sign Up", forState: UIControlState.Normal)
            registerButtom.text = "User Exists"
            signupButton.setTitle("Login", forState: UIControlState.Normal)
            signupActive = true
        }
        
         }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewDidAppear(animated: Bool) {
        if PFUser.currentUser() != nil {
            
            self.performSegueWithIdentifier("login", sender: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

