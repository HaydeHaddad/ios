//
//  Cell.swift
//  ParseStarterProject
//
//  Created by Hayden Haddad on 12/25/15.
//  Copyright © 2015 Parse. All rights reserved.
//

import UIKit

class Cell: UITableViewCell {

    @IBOutlet weak var postedImage: UIImageView!

    @IBOutlet weak var username: UILabel!
    
    @IBOutlet weak var message: UILabel!
    
}
