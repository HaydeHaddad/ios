//
//  TableViewController.swift
//  ParseStarterProject
//
//  Created by Hayden Haddad on 12/14/15.
//  Copyright © 2015 Parse. All rights reserved.
//

import UIKit
import Parse

class TableViewController: UITableViewController {
    
    var username = [""]
    var userIds = [""]
    var isFollowing = ["":false]
    var refresher: UIRefreshControl!
    
    
    func refresh() {
        
        let query = PFUser.query()
        
        query?.findObjectsInBackgroundWithBlock({ (objects, error) -> Void in
            
            if let users = objects {
                
                self.username.removeAll(keepCapacity: true)
                self.userIds.removeAll(keepCapacity: true)
                self.isFollowing.removeAll(keepCapacity: true)
                
                for object in users {
                    
                    if let user = object as? PFUser {
                        
                        if user.objectId != PFUser.currentUser()?.objectId {
                            
                            self.username.append(user.username!)
                            self.userIds.append(user.objectId!)
                            
                            let query = PFQuery(className: "Followers")
                            
                            query.whereKey("follower", equalTo: (PFUser.currentUser()?.objectId)!)
                            query.whereKey("following", equalTo: user.objectId!)
                            
                            query.findObjectsInBackgroundWithBlock({ (objects, error) -> Void in
                                
                                if let objects = objects {
                                    
                                    if objects.count > 0 {
                                        
                                        self.isFollowing[user.objectId!] = true
                                        
                                    } else {
                                        
                                        self.isFollowing[user.objectId!] = false
                                    }
                                }
                                
                                if self.isFollowing.count == self.username.count {
                                    
                                    self.tableView.reloadData()
                                    
                                  self.refresher.endRefreshing()
                                    
                                }
                                
                            })
                        }
                    }
                }
            }
            
        })
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
// ----------------------- add referesher --------------------

   refresher = UIRefreshControl()
   refresher.attributedTitle = NSAttributedString(string: "Pull to referesh")
   refresher.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
   self.tableView.addSubview(refresher)
   refresh()
//------------------------- referesher code end -----------------------

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return username.count
    }

   
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)

        cell.textLabel?.text = username[indexPath.row]
        
        let followingObjectId = userIds[indexPath.row]
        
        if isFollowing[followingObjectId] == true {
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
        
        return cell
    }
  

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let followingObjectId = userIds[indexPath.row]
        
        let cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        
        if isFollowing[followingObjectId] == false {
            
            isFollowing[followingObjectId] = true
            
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        
            let following = PFObject(className: "Followers")
        
            following["following"] = userIds[indexPath.row]
            following["follower"] = PFUser.currentUser()?.objectId
            following.saveInBackground()
        } else {
            
            isFollowing[followingObjectId] = false
            cell.accessoryType = UITableViewCellAccessoryType.None
            
            
            let query = PFQuery(className: "Followers")
            
            query.whereKey("follower", equalTo: (PFUser.currentUser()?.objectId)!)
            query.whereKey("following", equalTo: userIds[indexPath.row])
            let following = PFObject(className: "Followers")
            following.saveInBackground()
            query.findObjectsInBackgroundWithBlock({ (objects, error) -> Void in
                
                if let objects = objects {
                    
                    for object in objects {
                        
                        object.deleteInBackground()
                    }
                }
            })
        }
    }
}
