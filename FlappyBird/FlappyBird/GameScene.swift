//
//  GameScene.swift
//  FlappyBird
//
//  Created by Hayden Haddad on 2/10/16.
//  Copyright (c) 2016 hayden. All rights reserved.
//

import SpriteKit

class GameScene: SKScene, SKPhysicsContactDelegate {
   
    
  var score = 0
  var scoreLabel = SKLabelNode()
  
  var bird = SKSpriteNode()
  var bg = SKSpriteNode()
  
  var pipe1 = SKSpriteNode()
  var pipe2 = SKSpriteNode()
    
  var objectsContainer = SKSpriteNode()
  var labelsContainer = SKSpriteNode()
  
  enum ColliderType: UInt32 {
        
       case Bird = 1
       case Object = 2
       case Gap = 4
    }

    var gameOver = false
    
    func gameoverMessage() {
        
        let gameoverLabel = SKLabelNode()
        gameoverLabel.fontName = "Helvetica"
        gameoverLabel.fontSize = 30
        gameoverLabel.color = UIColor.redColor()
        gameoverLabel.text = "Gameover - Tap to play again"
        gameoverLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
        
       labelsContainer.addChild(gameoverLabel)
    }
    
    
    func background() {
        
        let bgTexture = SKTexture(imageNamed: "bg.png")
        
        let movebg = SKAction.moveByX(-bgTexture.size().width, y: 0, duration: 9)
        
        let repalcebg = SKAction.moveByX(bgTexture.size().width, y: 0, duration: 0)
        
        let movebgforever = SKAction.repeatActionForever(SKAction.sequence([movebg, repalcebg]))
        
        
        for i in 0 ..< 3 {
            
            
            bg = SKSpriteNode(texture: bgTexture)
            
            bg.position = CGPoint(x: bgTexture.size().width/2 + bgTexture.size().width * CGFloat(i), y: CGRectGetMidX(self.frame))
            
            bg.size.height = self.frame.height
            
            bg.runAction(movebgforever)
            
            objectsContainer.addChild(bg)
            
        }

        
    }
    
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        
        self.physicsWorld.contactDelegate = self
        
            addChild(objectsContainer)
            addChild(labelsContainer)
        
            background()
        
            scoreLabel.fontName = "Helvetica"
            scoreLabel.fontSize = 60
            scoreLabel.text = "0"
            scoreLabel.position = CGPointMake(CGRectGetMidX(self.frame), self.frame.size.height - 70)
            self.addChild(scoreLabel)
        
        
        
        
        let birdTexture = SKTexture(imageNamed: "flappy1.png")
        let birdTexture2 = SKTexture(imageNamed: "flappy2.png")
        
        let animation = SKAction.animateWithTextures([birdTexture,birdTexture2], timePerFrame: 0.1)
        
        let makeBirdFlap = SKAction.repeatActionForever(animation)
        
        bird = SKSpriteNode(texture: birdTexture)
        
        bird.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidX(self.frame))
        
        bird.runAction(makeBirdFlap)
        
        
        bird.physicsBody = SKPhysicsBody(circleOfRadius: birdTexture.size().height/2)
        bird.physicsBody!.dynamic = true
        
        bird.physicsBody!.categoryBitMask = ColliderType.Bird.rawValue
        bird.physicsBody!.contactTestBitMask = ColliderType.Object.rawValue
        bird.physicsBody!.collisionBitMask = ColliderType.Object.rawValue // this is for more complicated games (hit walls or trees ..etc)
        
        self.addChild(bird)
        
        let ground = SKNode()
        ground.position = CGPointMake(0, 0)
        ground.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(self.frame.size.width, 1))
        ground.physicsBody?.dynamic = false
        
        ground.physicsBody!.categoryBitMask = ColliderType.Object.rawValue
        ground.physicsBody!.contactTestBitMask = ColliderType.Object.rawValue
        ground.physicsBody!.collisionBitMask = ColliderType.Object.rawValue
        
        self.addChild(ground)
        
        _ = NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: #selector(GameScene.makePipes), userInfo: nil, repeats: true)
    
    }
    
    func makePipes() {
        
        let frameHight = self.frame.size.height
        let frameWidth = self.frame.size.width
        
        let gapHight = bird.size.height * 4
        
        let movmentAmount = arc4random() % UInt32(frameHight / 2)
        
        let pipeOffset = CGFloat(movmentAmount) - (frameHight / 4)
        
        let movePipes = SKAction.moveByX(-frameWidth * 2, y: 0,  duration: NSTimeInterval(frameWidth / 100))
        let removePipes = SKAction.removeFromParent()
        let moveAndRemovePipes = SKAction.sequence([movePipes,removePipes])
        
        let PipeTexture = SKTexture(imageNamed: "pipe1.png")
        let pipe1 = SKSpriteNode(texture: PipeTexture)
        
        let midScreenX = CGRectGetMidX(self.frame)
        let midScreenY = CGRectGetMidY(self.frame)
        
        
        
        pipe1.position = CGPoint(x:midScreenX + frameWidth , y: midScreenY + PipeTexture.size().height / 2 + gapHight / 2 + pipeOffset )
        pipe1.runAction(moveAndRemovePipes)
        
        pipe1.physicsBody = SKPhysicsBody(rectangleOfSize: PipeTexture.size())
        
        pipe1.physicsBody!.dynamic = false
        
        pipe1.physicsBody!.categoryBitMask = ColliderType.Object.rawValue
        pipe1.physicsBody!.contactTestBitMask = ColliderType.Object.rawValue
        pipe1.physicsBody!.collisionBitMask = ColliderType.Object.rawValue
        
        objectsContainer.addChild(pipe1)
        
        let Pipe2Texture = SKTexture(imageNamed: "pipe2.png")
        let pipe2 = SKSpriteNode(texture: Pipe2Texture)
        
        pipe2.position = CGPoint(x:midScreenX + frameWidth, y:midScreenY - PipeTexture.size().height / 2 - gapHight / 2 + pipeOffset)
        pipe2.runAction(moveAndRemovePipes)
        
        pipe2.physicsBody = SKPhysicsBody(rectangleOfSize: Pipe2Texture.size())
        
        pipe2.physicsBody!.dynamic = false
        
        pipe2.physicsBody!.categoryBitMask = ColliderType.Object.rawValue
        pipe2.physicsBody!.contactTestBitMask = ColliderType.Object.rawValue
        pipe2.physicsBody!.collisionBitMask = ColliderType.Object.rawValue
        
        objectsContainer.addChild(pipe2)
        
        let gap = SKNode()
        gap.position = CGPoint(x: midScreenX + frameWidth, y: midScreenY + pipeOffset)
        gap.runAction(moveAndRemovePipes)
        gap.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(pipe1.size.width, gapHight))
        gap.physicsBody!.dynamic = false
        
        gap.physicsBody!.categoryBitMask = ColliderType.Gap.rawValue
        gap.physicsBody!.contactTestBitMask = ColliderType.Bird.rawValue
        gap.physicsBody!.collisionBitMask = ColliderType.Gap.rawValue
        
        objectsContainer.addChild(gap)
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        
        if contact.bodyA.categoryBitMask == ColliderType.Gap.rawValue || contact.bodyB.categoryBitMask == ColliderType.Gap.rawValue {
            
            score += 1
            scoreLabel.text = String(score)
            
        } else {
        
            if gameOver == false {
                
                gameOver = true
                self.speed = 0
            
                gameoverMessage()

            }
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        if gameOver == false {
      
            bird.physicsBody!.velocity = CGVectorMake(0, 0)
            bird.physicsBody!.applyImpulse(CGVectorMake(0, 59))
        
        } else {
            
            score = 0
            
            scoreLabel.text = "0"
            
            bird.physicsBody!.velocity = CGVectorMake(0, 0)
            
            bird.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
            
            bird.physicsBody?.allowsRotation = false
            
            objectsContainer.removeAllChildren()
            
            background()
            
            self.speed = 1
            
            gameOver = false
            
            labelsContainer.removeAllChildren()
        }
 
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
