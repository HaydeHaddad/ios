//
//  ViewController.swift
//  ReturnWebContent
//
//  Created by Hayden Haddad on 10/25/15.
//  Copyright © 2015 hayden. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let url = NSURL(string: "http://stackoverflow.com")!
 /* only request web *********/
       /* webView.loadRequest(NSURLRequest(URL:url))*/
 /* only request web *********/
 
 /* Download webpage *************/
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) -> Void in
          
            if let urlContent = data {
               let webContent = NSString(data: urlContent, encoding: NSUTF8StringEncoding)
                
                dispatch_async(dispatch_get_main_queue(),{ ()
                    
                self.webView.loadHTMLString(String(webContent!), baseURL: nil)
                    
                })
                
                
              } else {
                
           }
        }
       task.resume()
     /* Download webpage *********************/

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

