//
//  ViewController.swift
//  Swipes & Shakes
//
//  Created by Hayden Haddad on 11/12/15.
//  Copyright © 2015 hayden. All rights reserved.
//

import UIKit
import AVFoundation
import Darwin
class ViewController: UIViewController {

    var audioPlayer: AVAudioPlayer = AVAudioPlayer()
    var sounds = ["bell","dragBell","elecBell"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let swipeRight = UISwipeGestureRecognizer(target: self, action: "swiped:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: "swiped:")
        swipeUp.direction = UISwipeGestureRecognizerDirection.Up
        self.view.addGestureRecognizer(swipeUp)
        
    }
    
    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent?) {
        if event?.subtype == UIEventSubtype.MotionShake {
            
            let randomNumber = Int(arc4random_uniform(UInt32(sounds.count)))
       
                
           let filePath = NSBundle.mainBundle().pathForResource(sounds[randomNumber], ofType: "mp3")
  
            do
            {
             try audioPlayer = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: filePath!))
            }catch {
                
            }
            audioPlayer.play()
        }
    }
}
    
    func swiped (gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture  as? UISwipeGestureRecognizer  {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                print("swiped Right")
                
            case UISwipeGestureRecognizerDirection.Up:
                print("Swiped Up")
                
            default:
                break
        }
    }
}


