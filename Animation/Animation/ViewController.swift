//
//  ViewController.swift
//  Animation
//
//  Created by Hayden Haddad on 10/29/15.
//  Copyright © 2015 hayden. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var counter = 1
    var counter2 = 1
    var timer = NSTimer()
    var isAnimating = true

    @IBOutlet weak var hamester: UIImageView!
    @IBOutlet weak var snake: UIImageView!
    @IBOutlet weak var image: UIImageView!
    @IBAction func imageButton(sender: AnyObject) {
        
        if isAnimating == true {
            timer.invalidate()
            isAnimating = false
        } else {
            timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector:Selector("doAnimation"), userInfo: nil, repeats: true)
        isAnimating = true
        
        }

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector:Selector("doAnimation"), userInfo: nil, repeats: true)
    }
    
        func doAnimation() {
            
            if  counter == 5 {
                counter = 1
            } else
            {
                counter++
                image.image = UIImage(named: "fram\(counter).png")
            }
            
            if  counter2 == 3 {
                counter2 = 1
            } else
            {
                counter2++
                image.image = UIImage(named: "fram\(counter2).png")
                snake.image = UIImage(named: "snak\(counter2).png")
                hamester.image = UIImage(named: "hames\(counter2).png")
            }
            
            
    }
    
    
/*
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        image.frame = CGRect(x: 100, y: 20, width: 0, height: 0)
    }

    override func viewDidAppear(animated: Bool) {
        UIView.animateWithDuration(1) { () -> Void in
             self.image.frame = CGRect(x: 100, y: 20, width: 100, height: 200)
        }

       
    }
*/
    
}

